package clases;

public class Planetas {
	private int numeroDeSatelites;
	private int masaPlaneta;
	private String estrella;
	private String nombre;
	private int distanciaHastaEstrella;

	public int getDistanciaHastaEstrella() {
		return distanciaHastaEstrella;
	}

	public void setDistanciaHastaEstrella(int distanciaHastaEstrella) {
		this.distanciaHastaEstrella = distanciaHastaEstrella;
	}

	public Planetas() {
		this.nombre = "Tierra";
		this.numeroDeSatelites = 8;
		this.masaPlaneta = 198000000;
	}

	public Planetas(String estrella) {
		super();
		this.estrella = estrella;
	}

	public Planetas(int numeroDeSatelites, String estrella) {
		super();
		this.numeroDeSatelites = numeroDeSatelites;
		this.estrella = estrella;
	}

	public Planetas(int numeroDeSatelites, String estrella, int masaPlaneta, String nombre,
			String distanciaHastaEstrella) {
		super();
		this.numeroDeSatelites = numeroDeSatelites;
		this.masaPlaneta = masaPlaneta;
		this.estrella = estrella;
		this.nombre = nombre;
	}

	public int getNumeroDeSatelites() {
		return numeroDeSatelites;
	}

	public void setNumeroDeSatelites(int numeroDeSatelites) {
		this.numeroDeSatelites = numeroDeSatelites;
	}

	public int getMasaPlaneta() {
		return masaPlaneta;
	}

	public void setMasaPlaneta(int masaPlaneta) {
		this.masaPlaneta = masaPlaneta;
	}

	public String getEstrella() {
		return estrella;
	}

	public void setEstrella(String estrella) {
		this.estrella = estrella;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "Planetas [numeroDeSatelites=" + numeroDeSatelites + ", masaPlaneta=" + masaPlaneta + ", estrella="
				+ estrella + ", nombre=" + nombre + ", distanciaHastaEstrella=" + distanciaHastaEstrella + "]";
	}

	// metodo que si tiene mas de 100000 se le cambia el nombre a ***_PE de pesado
	public static void RenombrePorMasaPlaneta(Planetas planeta) {
		if (planeta.getMasaPlaneta() > 100000) {
			String nuevoNombre = planeta.getNombre() + "_PE";
			planeta.setNombre(nuevoNombre);
		}

	}

	// Metodo que nos muestra cuanto tiempo se tardar�a en ir desde el planeta hasta
	// la estrella
	// dado la velocidad (km/h) que se de por parametro y y la cantidad de agua que
	// necesitarian dado el numero de astronautas
	// siendo que cada astronauta bebe 3 litros al d�a y unos para el aseo personal
	public void CalcularTiempoHastaLlegarAEstrella(double velocidad, int astronautas) {

		double tiempo = this.distanciaHastaEstrella / velocidad;
		System.out.println("Tardar�an " + tiempo + " horas en llegar");
		double litros = (tiempo / 24) * (astronautas * 4);
		System.out.println("El numero de litros que necesitarian seria de " + litros);
	}
}
package clases;

public class PlanetasExternos extends Planetas {
	private static final int RADIO_SOL = 69;
	private static final double VOLUMEN_SOL = (4 / 3) * Math.pow(RADIO_SOL, 3) * Math.PI;
	private int gasMasAbundante;
	private double radio;
	private String nombre;

	public int getGasMasAbundante() {
		return gasMasAbundante;
	}

	public void setGasMasAbundante(int gasMasAbundante) {
		this.gasMasAbundante = gasMasAbundante;
	}

	public double getRadio() {
		return radio;
	}

	public void setRadio(double radio) {
		this.radio = radio;
	}

	public PlanetasExternos() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PlanetasExternos(int numeroDeSatelites, String estrella, int masaPlaneta, String nombre,
			String distanciaHastaEstrella) {
		super(numeroDeSatelites, estrella, masaPlaneta, nombre, distanciaHastaEstrella);
		// TODO Auto-generated constructor stub
	}

	public PlanetasExternos(int numeroDeSatelites, String estrella) {
		super(numeroDeSatelites, estrella);
		// TODO Auto-generated constructor stub
	}

	public PlanetasExternos(String estrella) {
		super(estrella);
		// TODO Auto-generated constructor stub
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double CalcularVolumen() {
		double volumen = (4 / 3) * Math.pow(this.radio, 3) * Math.PI;
		System.out.println("el volumen del planeta " + super.getNombre() + " es " + volumen);
		return volumen;

	}

	// te dice cuantas veces un planeta equivale al tamanno del sol
	public void VecesPlanetaSol() {
		double vecesRadio = RADIO_SOL / this.radio;
		double vecesTamanno = VOLUMEN_SOL / CalcularVolumen();
		System.out.println(vecesRadio + " veces el radio del planeta " + this.nombre + " equivale al radio del sol");
		System.out.println(
				vecesTamanno + " veces el volumen del planeta " + this.nombre + " equivale al volumen del sol");
	}

	@Override
	public String toString() {
		return "PlanetasExternos [gasMasAbundante=" + gasMasAbundante + ", radio=" + radio + ", nombre=" + nombre
				+ "numeroDeSatelites=" + super.getNumeroDeSatelites() + ", masaPlaneta=" + super.getMasaPlaneta()
				+ ", estrella=" + super.getEstrella() + ", distanciaHastaEstrella=" + super.getDistanciaHastaEstrella()
				+ "]";
	}

}

package clases;

public class PlanetasInternos extends Planetas {
	private String rocasMasAbundantes;
	private double densidad;
	private String estrella;

	public PlanetasInternos(String rocasMasAbundantes, double densidad, String estrella) {
		super();
		this.rocasMasAbundantes = rocasMasAbundantes;
		this.densidad = densidad;
		this.estrella = estrella;
	}

	public PlanetasInternos() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PlanetasInternos(int numeroDeSatelites, String estrella, int masaPlaneta, String nombre,
			String distanciaHastaEstrella) {
		super(numeroDeSatelites, estrella, masaPlaneta, nombre, distanciaHastaEstrella);
		// TODO Auto-generated constructor stub
	}

	public PlanetasInternos(int numeroDeSatelites, String estrella) {
		super(numeroDeSatelites, estrella);
		// TODO Auto-generated constructor stub
	}

	public PlanetasInternos(String estrella) {
		super(estrella);
		// TODO Auto-generated constructor stub
	}

	public String getRocasMasAbundantes() {
		return rocasMasAbundantes;
	}

	public void setRocasMasAbundantes(String rocasMasAbundantes) {
		this.rocasMasAbundantes = rocasMasAbundantes;
	}

	public double getDensidad() {
		return densidad;
	}

	public void setDensidad(double densidad) {
		this.densidad = densidad;
	}

	public String getEstrella() {
		return estrella;
	}

	public void setEstrella(String estrella) {
		this.estrella = estrella;
	}

	public void MostrarColorPlaneta() {
		if (this.rocasMasAbundantes.equalsIgnoreCase("feldespato")) {
			System.out.println("El color del planeta " + getNombre() + " es blanco");
		} else {
			System.out.println(
					"No se puede saber con exactitud debido a que los otros minerales tienen una amplia gama de colores");
		}
	}

	@Override
	public String toString() {
		return "PlanetasInternos [rocasMasAbundantes=" + rocasMasAbundantes + ", densidad=" + densidad + ", estrella="
				+ estrella + "numeroDeSatelites=" + super.getNumeroDeSatelites() + ", masaPlaneta="
				+ super.getMasaPlaneta() + ", nombre=" + super.getNombre() + ", distanciaHastaEstrella="
				+ super.getDistanciaHastaEstrella() + "]";
	}

	// Metodo que dice si la densidad est� por encima de la media, siendo la media
	// de 23
	public void DensidadMedia() {
		if (this.densidad > 23) {
			System.out.println("La densidad del planeta " + super.getNombre() + " es mayor que la media");
		} else if (this.densidad < 23) {
			System.out.println("La densidad del planeta " + super.getNombre() + " es menor que la media");
		} else {
			System.out.println("La densidad del planeta " + super.getNombre() + " es igual que la media");
		}
	}

}
